/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serwlety;

import ejb.UslugaSOAP;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author Mefju
 */
public class Klient extends HttpServlet {

    @WebServiceRef(wsdlLocation = "http://localhost:8080/UslugaSOAP/Usluga?WSDL")
    private UslugaSOAP service;

    

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Średnia harmoniczna - klient</title>");            
            out.println("<link rel=\"stylesheet\" href=\"styl.css\" type=\"text/css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<div class=\"central\">");
            out.println("<h1>Obliczanie średniej harmonicznej</h1>");
            
            
            String input = request.getParameter("dane");
            
            if(input==null || input.isEmpty()){ // jeśli użytkownik nie podał danych
                out.println("Nie podano danych, więc zostały wylosowane.<br/>");
                out.println("Wygenerowane liczby, to: <br/>");
                Random rng = new Random();
                int n = rng.nextInt(9) + 2;        // ile liczb 2-10
                List<Double> dane = new ArrayList<>();
                out.println("<table style=\"margin: auto\"><tr>");
                for(int i = 0; i < n; i++){
                    double los = rng.nextDouble()*10;
                    dane.add(los); //n doubli 0-10
                    out.println("<td  style=\"border: 1px solid black; padding: 5px 15px\">" + new DecimalFormat("0.####").format(los) + "</td>");
                }
                
                out.println("</tr></table>");
                
                //out.println(Arrays.toString(dane.toArray()));
                out.println("<br/>Średnia harmoniczna: <br/><h2>" + new DecimalFormat("0.####").format(sredniaHarmoniczna(dane)) + "</h2>");
                
            } else {      // jeśli użytkownik podał dane
                String[] numbers = input.trim().replaceAll(" +", " ").split(" ");
                List<Double> dane = new ArrayList<>();
                String table = "<table style=\"margin:auto\"><tr>";
                String error = new String();
                try{
                    for(String s : numbers){
                        s = s.replace(",", ".").trim();
                        error = s;
                        double d = Double.parseDouble(s);
                        dane.add(d);
                        table += "<td  style=\"border: 1px solid black; padding: 5px 15px\">" + new DecimalFormat("0.####").format(d) + "</td>";
                    }
                    table += "</tr></table>";
                    out.println("<br/>Podane liczby, to: <br/>");
                    out.println(table);
                    out.println("<br/>Średnia harmoniczna: <br/><h2>" + new DecimalFormat("0.####").format(sredniaHarmoniczna(dane)) + "</h2>");
                    
                } catch (NumberFormatException e){ // jeśli nie da się sparsować, pokaż gdzie jest błąd
                    out.println("Podano błędne dane!<br/> '" + error + "' nie jest poprawną liczbą.");
                }
            }
            
            
            out.println("<form action=\"index.html\">");
            out.println("<input type=\"submit\" value=\"Jeszcze raz!\" class=\"button\"/>");
            out.println("</form>");
            
            out.println("</div>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private double sredniaHarmoniczna(java.util.List<java.lang.Double> dane) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        ejb.Usluga port = service.getUslugaPort();
        return port.sredniaHarmoniczna(dane);
    }

    

}

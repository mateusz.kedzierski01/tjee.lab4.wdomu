package ejb;

import javax.ejb.Local;

@Local
public interface ObliczeniaLocal {
    double sredniaHarmoniczna(double[] dane);
}

package ejb;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.LocalBean;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;


@WebService(serviceName = "UslugaSOAP")
@Singleton
@LocalBean
public class Usluga {

    @EJB
    private ObliczeniaLocal obliczenia;

    
    @WebMethod(operationName = "sredniaHarmoniczna")
    public double sredniaHarmoniczna(@WebParam(name = "dane") double[] dane) {
        return obliczenia.sredniaHarmoniczna(dane);
    }
}

package ejb;

import javax.ejb.Stateless;

@Stateless
public class Obliczenia implements ObliczeniaLocal {

    @Override
    public double sredniaHarmoniczna(double[] dane) {
        int n = dane.length;
        double mianownik = 0;
        for(double d : dane){
            mianownik += 1/d;
        }
        return ((double)n)/mianownik;
    }
    
}
